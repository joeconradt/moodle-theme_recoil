Moodle Recoil Theme - By Joseph Conradt

Full documentation: http://docs.moodle.org/24/en/Recoil_Theme

Moodle licensing info: http://docs.moodle.org/dev/License

**How To Install**

1. Download the theme from https://moodle.org/plugins/view.php?plugin=theme_recoil.
2. Extract and upload to your Moodle code in the folder moodle/theme.
3. Log in as an Administrator in your Moodle site, you should be prompted to update the database. (Note that you may have to go to the home page).
4. While still logged in as an Administrator, go to Site administration->Appearance->Themes->Theme selector.
5. Near the Default theme choose Change theme.
6. Scroll down and find Recoil, choose Use theme.

--------------------------------------------

**Update 4-12-13** v0.9.3

Replaced fixed footer with relative footer. 

Fixed black boxes around content.

--------------------------------------------

**Update 4-19-13** v1.0

Fixed header logo and heading.

Added footer logo support.


