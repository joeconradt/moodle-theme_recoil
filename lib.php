<?php
/**
 * Theme settings functions
 *
 * @package    theme
 * @subpackage recoil
 * @copyright  2013 Joseph Conradt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function recoil_set_logo($css, $logoURL) {
    $tag = '[[setting:image]]';
    $replacement = $logoURL;
    if (is_null($replacement)) {
        $replacement = 'http://conradtweb.com/files/images/logo2.png';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function recoil_set_footerlogo($css, $logoURL) {
    $tag = '[[setting:footerlogo]]';
    $replacement = $logoURL;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function recoil_process_css($css, $theme)
{

    if (!empty($theme->settings->logo)) {
        $logo = $theme->settings->logo;
    } else {
        $logo = null;
    }
    $css = recoil_set_logo($css, $logo);

    if (!empty($theme->settings->footerlogo)) {
        $logo = $theme->settings->footerlogo;
    } else {
        $logo = null;
    }
    $css = recoil_set_footerlogo($css, $logo);

	return $css;
}