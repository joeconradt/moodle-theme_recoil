<?php
 
/*
*
* Settings for theme_recoil
*
*/
 
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
 
	// Logo file setting
	$name = 'theme_recoil/logo';
	$title = "URL to header logo";
	$description = "This is the URL to your logo which will appear at the top in your header.";
	$setting = new admin_setting_configtext($name, $title, $description, 'http://conradtweb.com/files/images/logo2.png', PARAM_URL);
	$settings->add($setting);
 	
 	// Footer logo setting
 	$name = 'theme_recoil/footerlogo';
	$title = "URL to footer logo";
	$description = "URL to the logo that will appear in the footer.";
	$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
	$settings->add($setting);

	// Footer logo setting
 	$name = 'theme_recoil/footerlogotarget';
	$title = "Footer logo link";
	$description = "The URL that the footer logo will link to.";
	$setting = new admin_setting_configtext($name, $title, $description, 'http://www.elightenmentlearning.com/moodle/', PARAM_URL);
	$settings->add($setting);
}
?>